#!/bin/bash

while ! head -n1 library.bib | grep -q '@'
do
	sed -i '1d' library.bib
done
echo "the menedeley preamble has been successfully removed"
# cat references.bib
